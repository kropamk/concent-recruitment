Zbudowac prosta stronke z wykorzystaniem React.js i TypeScript wedlug wymagan:
1.	Strona skladac ma sie z conajmniej 2 komponentow reactowych (zaznaczonych czerwona ramka: A i B. Ramka jest jedynie do zaznaczenia komponentow i nie powinna znalezc sie na stronie)
2.	Wszystkie texty na stronie powinny byc „hardcodowane”, czyli byc statyczne. Wszystkie kwoty powinny byc dynamiczne. Wartosci te powinny znalezc sie w oddzielnych plikach jsonowych, ktore po wejsciu na stronie powinienes wczytac, sparsowac na obiekty JavaScriptiowe i „zbindowac” to tych komponentow. Czyli np. Plik dataA.json majacy dane dla komponentu A i dataB.json majacy dane dla komponentu B. W momencie zmiany wartosci w pliku jsonowym, po odswiezeniu strony nowe wartosci powinny byc wyswietlone na stronie.
3.	Klikajac na przycisk Direct payment lub Withdraw case w komponencie A tekst tego przycisku powinien byc wyswietlony w komponencie B w miejscu obecnego tekstu „Olav Olsen”.

