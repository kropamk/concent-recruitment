import React from 'react';

import Case from './components/case/Case';
import Debtor from './components/debtor/Debtor';

import { CaseProvider } from './contexts/CaseContext';
import { DebtorProvider } from './contexts/DebtorContext';

interface State {
  case: any,
  debtor: any,
  loading: boolean,
  textBtn: string,
  activeBtnId: string
}

class App extends React.Component<any, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      loading: true,
      textBtn: 'special button value',
      activeBtnId: null,
      debtor: {},
      case: {}
    };
  }

  componentDidMount() {
    this.getData().then(([a, b]) => {
      this.setState({...this.state, loading: false, case: a, debtor: b});
    });
  }

  getData = async() => {
    return await Promise.all([
      fetch('./mock/mockA.json')
        .then((res) => res.json())
        .then((data) => {
          return data;
        }, () => {
        }).catch(() => {
        }),
      fetch('./mock/mockB.json')
        .then((res) => res.json())
        .then((data) => {
          return data;
        })
    ]);
  }

  updateDebtorName = (id: string, text: string) => {
    this.setState({ 
      ...this.state,
      activeBtnId: id,
      debtor: { ...this.state.debtor, name: text} }  
    );
  }

  render() {
    return (
      <div className="l-main" role="main">
        {!this.state.loading ? 
          <CaseProvider value={{
            data: this.state.case.data,
            paymentsData: this.state.case.paymentsData,
            actions: this.state.case.actions,
          }}>
            <DebtorProvider value={{
              name: this.state.debtor.name,
              ref: this.state.debtor.ref,
              id: this.state.debtor.id,
              phone: this.state.debtor.phone,
              address: this.state.debtor.address,
              activeBtnId: this.state.activeBtnId,
              updateDebtorName: this.updateDebtorName
            }}>
              <section className="l-section">
                <Case />
                <Debtor />
              </section>
            </DebtorProvider>
          </CaseProvider> : null
        }
      </div>
    );
  }
}

export default App;

