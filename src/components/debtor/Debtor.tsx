import React from 'react';
import { DebtorConsumer } from './../../contexts/DebtorContext';

interface State {
  debtor: any
}

class Debtor extends React.Component<{}, State> {
  constructor(props: null) {
    super(props);
    this.state = {
      debtor: {
        name: null
      }
    }
  }

  render() {
    return (
      <DebtorConsumer>
        {({ ...debtor }) => (
          <aside className="l-debtor">
            <section className="l-debtor__table c-table c-table--col-2">
              <div className="l-debtor__table-row c-table__row">
                <div className="c-table__cell font--color-1 font--500">Debtor</div>
                <div className="c-table__cell font--color-3 font--500">{debtor.name}</div>
              </div>
    
              <div className="l-debtor__table-row c-table__row">
                <div className="c-table__cell">Debtor ref.</div>
                <div className="c-table__cell">{debtor.ref}</div>
              </div>
    
              <div className="l-debtor__table-row c-table__row">
                <div className="c-table__cell">ID</div>
                <div className="c-table__cell">{debtor.id}</div>
              </div>
    
              <div className="l-debtor__table-row c-table__row">
                <div className="c-table__cell">Phone no.</div>
                <div className="c-table__cell">
                  <a href={"tel:" + debtor.phone}>{debtor.phone}</a>
                </div>
              </div>
    
              <div className="l-debtor__table-row c-table__row">
                <div className="c-table__cell">Address</div>
                <address className="c-table__cell">
                  {debtor.address.street} {debtor.address.houseNo} <br/>
                  {debtor.address.postCode} {debtor.address.city}
                </address>
              </div>
            </section>

            <section className="l-debtor__summary">
              <div className="l-debtor__summary-box c-box c-box--5">
                <div className="c-box__icon">$</div>
                <div className="c-box__amount">
                  <span className="c-box__amount-value">8,253.54</span>
                  <span className="c-box__currency">NOK</span>
                </div>
                <div className="c-box__desc">Total debet on all active cses</div>
              </div>
            </section>
          </aside>
       )}
      </DebtorConsumer>
    )
  }
}
      
export default Debtor;