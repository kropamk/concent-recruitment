import React from 'react';
import { CaseConsumer } from '../../contexts/CaseContext';
import ActionsBtn from './actions';

class Case extends React.Component<{}, {}> {
  constructor(props: {}) {
    super(props);
  }

  render() {
    return (
      <CaseConsumer>
        {({ data, paymentsData, actions }) => (
          <article className="l-case">
            <section className="l-case__info">
              <div className="c-table c-table--col-2">
                <div className="c-table__row">
                  <div className="c-table__cell font--color-1 font--500">Case no. / ref.</div>
                  <div className="c-table__cell color-1 font--color-2 font--500">95861372 / 75896</div>
                </div>
    
                <div className="c-table__row">
                  <div className="c-table__cell font--color-1 font--500">Status</div>
                  <div className="c-table__cell font--color-3 font--500">{data.status}</div>
                </div>
                
                <div className="c-table__row">
                  <div className="c-table__cell">Reg. date</div>
                  <div className="c-table__cell">{data.regDate}</div>
                </div>
    
                <div className="c-table__row">
                  <div className="c-table__cell">Close date</div>
                  <div className="c-table__cell">{data.closeDate}</div>
                </div>
    
                <div className="c-table__row">
                  <div className="c-table__cell font--color-1 font--500">Account no.</div>
                  <div className="c-table__cell font--color-3 font--500">{data.accountNrb}</div>
                </div>
    
                <div className="c-table__row">
                  <div className="c-table__cell font--color-1 font--500">KID</div>
                  <div className="c-table__cell font--color-3 font--500">{data.kid}</div>
                </div>
    
                <div className="c-table__row">
                  <div className="c-table__cell">Creditor</div>
                  <div className="c-table__cell font--color-1 font--500">{data.creditor}</div>
                </div>
    
                <div className="c-table__row">
                  <div className="c-table__cell">Creditor ref.</div>
                  <div className="c-table__cell">{data.creditorRef}</div>
                </div>
    
                <div className="c-table__row">
                  <div className="c-table__cell">Invoices</div>
                  <div className="c-table__cell">{data.invoices}</div>
                </div>
    
                <div className="c-table__row">
                  <div className="c-table__cell">Case handler</div>
                  <div className="c-table__cell">{data.caseHandler}</div>
                </div>
              </div>
            </section>

            <section className="l-case__payments">
              <div className="l-case__payments-box c-box c-box--1">
                <div className="c-box__icon">$</div>
                <div className="c-box__amount">
                  <span className="c-box__amount-value">{paymentsData.claim}</span>
                  <span className="c-box__currency">NOK</span>
                </div>
                <div className="c-box__desc">Original claim</div>
              </div>

              <div className="l-case__payments-box c-box c-box--2">
                <div className="c-box__icon">$</div>
                <div className="c-box__amount">
                  <span className="c-box__amount-value">{paymentsData.onCase}</span> 
                  <span className="c-box__currency">NOK</span>
                </div>
                <div className="c-box__desc">Payment on case</div>
              </div>

              <div className="l-case__payments-box c-box c-box--3">
                <div className="c-box__icon">$</div>
                <div className="c-box__amount">
                  <span className="c-box__amount-value">{paymentsData.pending}</span>
                  <span className="c-box__currency">NOK</span>
                </div>
                <div className="c-box__desc">Pending payment</div>
              </div>

              <div className="l-case__payments-box c-box c-box--4">
                <div className="c-box__icon">$</div>
                <div className="c-box__amount">
                  <span className="c-box__amount-value">{paymentsData.interests}</span>
                  <span className="c-box__currency">NOK</span>
                </div>
                <div className="c-box__desc">Interests</div>
              </div>
            </section>

            <section className="l-case__actions">
              {Object.keys(actions).map((actionName: string, index: number) => {
                const actionsArr:any = actions;

                if (actionsArr[actionName].visible) {
                  return <ActionsBtn key={index} name={actionName} data={actionsArr[actionName]} />;
                }
              })}
            </section>
          </article>
        )}
      </CaseConsumer>
    )
  }
}

export default Case;