import React from 'react';
import Icon from './icons';

export interface Props {
    allActiveCases: number;
};

class SeeAllActiveCasesBtn extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <button className="c-btn c-btn--1 c-btn--xl">
        <Icon name="file"/>
        See all active sases ({this.props.allActiveCases})
      </button>
    )
  }
}
      
export default SeeAllActiveCasesBtn;