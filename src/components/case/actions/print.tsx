import React from 'react';
import Icon from './icons';

class PrintBtn extends React.Component<{}, {}> {
  render() {
    return (
      <button className="c-btn c-btn--1 c-btn--xl">
        <Icon name="printer"/>
        Print case
      </button>
    )
  }
}
      
export default PrintBtn;