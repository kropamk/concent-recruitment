import React from 'react';
import { DebtorConsumer } from './../../../contexts/DebtorContext';
import Icon from './icons';

class DirectPaymentBtn extends React.Component<{}, {}> {
  render() {
    return (
      <DebtorConsumer>
        {({ activeBtnId, updateDebtorName }) => (
          <button className={"c-btn c-btn--1 c-btn--xl " + (activeBtnId === 'directPayment' ? 'c-btn--active' : '')} 
                  id="directPayment"
                  value="Directs payment / Credit note"
                  onClick={(e) => updateDebtorName('directPayment', (e.target as HTMLInputElement).value)}>
            <Icon name="credit-card" fill={activeBtnId === 'directPayment' ? '#fff' : ''} width={32} height={26}/>
            Directs payment / Credit note
          </button>
        )}
      </DebtorConsumer>
    )
  }
}
      
export default DirectPaymentBtn;