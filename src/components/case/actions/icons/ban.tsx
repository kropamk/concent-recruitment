import React from 'react';

export interface Props {
  width: number;
  height: number;
  fill: string;
};

class BanSvg extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const {width, height, fill} = this.props;

    return (
      <svg 
        version="1.1" 
        id="Capa_1" 
        xmlns="http://www.w3.org/2000/svg" 
        x="0px" 
        y="0px"
        width={width} 
        height={height}
        viewBox="0 0 512 512">
        <g>
          <g>
            <path fill={fill} d="M256,0C114.84,0,0,114.84,0,256s114.84,256,256,256s256-114.84,256-256S397.16,0,256,0z M256,474.718
                C135.4,474.718,37.282,376.6,37.282,256S135.4,37.282,256,37.282S474.718,135.4,474.718,256S376.6,474.718,256,474.718z"/>
          </g>
        </g>
        <g>
          <g>
            <rect fill={fill} x="24.881" y="237.964" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -106.466 256.1715)" width="462.225" height="37.275"/>
          </g>
        </g>
      </svg>
    )
  }
}
      
export default BanSvg;