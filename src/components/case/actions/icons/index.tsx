import React from 'react';
import CreditCardSvg from './credit-card';
import BanSvg from './ban';
import PrinterSvg from './printer';
import FileSvg from './file';

export interface Props {
  name: string;
  width?: number;
  height?: number;
  fill?: string;
};

class Icon extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const {name, width, height, fill} = this.props;
    let icon;

    if (name === 'credit-card') {
      icon = <CreditCardSvg fill={fill || null} width={width || 32} height={height || 26}/>;
    } else if (name === 'ban') {
      icon = <BanSvg fill={fill || null} width={width || 26} height={height || 26}/>;
    } else if (name === 'printer') {
      icon = <PrinterSvg fill={fill || null} width={width || 28} height={height || 26}/>;
    } else if (name === 'file') {
      icon = <FileSvg fill={fill || null} width={width || 22} height={height || 26}/>;
    }

    return (
      <div className="c-icon c-icon--case-action">
        {icon}
      </div>
    )
  }
}
      
export default Icon;