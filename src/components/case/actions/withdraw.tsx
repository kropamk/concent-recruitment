import React from 'react';
import { DebtorConsumer } from './../../../contexts/DebtorContext';
import Icon from './icons';

class WithDrawBtn extends React.Component<{}, {}> {
  render() {
    return (
      <DebtorConsumer>
        {({ activeBtnId, updateDebtorName }) => (
          <button className={"c-btn c-btn--1 c-btn--xl " + (activeBtnId === 'withdraw' ? 'c-btn--active' : '')} 
                  id="withdraw"
                  value="Withdraw case"
                  onClick={(e) => updateDebtorName('withdraw', (e.target as HTMLInputElement).value)}>
            <Icon name="ban" fill={activeBtnId === 'withdraw' ? '#fff' : ''}/>
            Withdraw case
          </button>
        )}
      </DebtorConsumer>
    )
  }
}
      
export default WithDrawBtn;