import React from 'react';

import DirectPaymentBtn from './direct-payment';
import WithDrawBtn from './withdraw';
import PrintBtn from './print';
import SeeAllActiveCasesBtn from './see-all-active-cases';

export interface Props {
  name: string;
  data: any;
};

class ActionsBtn extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const { name } = this.props;
    let btn;

    if (name === 'directPayment') {
      btn = <DirectPaymentBtn />;
    } else if (name === 'withdraw') {
      btn = <WithDrawBtn />;
    } else if (name === 'print') {
      btn = <PrintBtn />;
    } else if (name === 'seeAllActiveCases') {
      btn = <SeeAllActiveCasesBtn allActiveCases={this.props.data.count}/>;
    }

    return (
      <div className="l-case__action-btn">
        {btn}
      </div>
    )
  }
}
      
export default ActionsBtn;