import React from 'react';

interface CaseContextInterface {
  data: dataType;
  paymentsData: paymentsDataType;
  actions: actionsType;
}

type actionsType = {
  directPayment: {
    visible: boolean;
  },
  withdraw: {
    visible: boolean;
  },
  print: {
    visible: boolean;
  },
  seeAllActiveCases: {
    visible: boolean;
    count: number;
  },
}

type dataType = {
  status: string;
  regDate: string;
  closeDate: string;
  accountNrb: number;
  kid: number;
  creditor: string;
  creditorRef: string;
  invoices: number;
  caseHandler: string;
  allActiveCases: number;
}

type paymentsDataType = {
  claim: string;
  onCase: string;
  pending: string;
  interests: string;
}

const CaseContext = React.createContext<CaseContextInterface>(null);

export const CaseProvider = CaseContext.Provider;
export const CaseConsumer = CaseContext.Consumer;