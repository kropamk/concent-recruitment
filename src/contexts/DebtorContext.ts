import React from 'react';

interface DebtorContextInterface {
  name: string;
  ref: number;
  id: number;
  phone: number;
  address: addressProp,
  activeBtnId: string,
  updateDebtorName(id: string, text: string): void
}

type addressProp = {
  street: string;
  houseNo: number;
  postCode: number;
  city: string;
}

const DebtorContext = React.createContext<DebtorContextInterface>({
  name: null,
  ref: null,
  id: null,
  phone: null,
  address: null,
  activeBtnId: null,
  updateDebtorName: () => {
    throw new Error('dsada() not implemented');
  }
});

export const DebtorProvider = DebtorContext.Provider;
export const DebtorConsumer = DebtorContext.Consumer;