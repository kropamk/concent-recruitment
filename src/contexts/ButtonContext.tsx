import React from 'react';

interface ButtonContextInterface {
  debtorName: string,
  updateDebtorName(text: string): void
}

export const ButtonContext = React.createContext<ButtonContextInterface>({
  debtorName: null,
  updateDebtorName: () => {
    throw new Error('updateDebtorName() not implemented');
  }
});

export const ButtonProvider = ButtonContext.Provider;
export const ButtonConsumer = ButtonContext.Consumer;